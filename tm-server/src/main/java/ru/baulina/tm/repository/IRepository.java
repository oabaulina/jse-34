package ru.baulina.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.baulina.tm.entity.AbstractEntity;

public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, Long> {
}
